import $ from 'jquery';
import angular from 'angular';
import ngSanitize from 'angular-sanitize';
import ngAnimate from 'angular-animate';
import smartTable from 'angular-smart-table';
import checklistModel from 'checklist-model';

import ngModule from './module.js';
import dateFilter from './js/components/dateFilter/dateFilter.js';
import platformOpsFilter from './js/components/platformOpsFilter/platformOpsFilter.js';
import regionFilter from './js/components/regionFilter/regionFilter.js';
import officeFilter from './js/components/officeFilter/officeFilter.js';
import platformClientFilter from './js/components/platformClientFilter/platformClientFilter.js';
import agencyFilter from './js/components/agencyFilter/agencyFilter.js';
import billingStatsFilter from './js/components/billingStatsFilter/billingStatsFilter.js';
import deviceFilter from './js/components/deviceFilter/deviceFilter.js';
import clientStatsTable from './js/components/clientStatsTable/clientStatsTable.js';

import './css/styles.scss';

dateFilter(ngModule);
platformOpsFilter(ngModule);
regionFilter(ngModule);
officeFilter(ngModule);
platformClientFilter(ngModule);
agencyFilter(ngModule);
billingStatsFilter(ngModule);
deviceFilter(ngModule);
clientStatsTable(ngModule);

ngModule.controller('billingController', function($scope, clientStatsFactory){
    let self = this;
    
    self.platformOpsFilterData = {
        options: [],
        pickedOptions: []
    };
    self.regionFilterData = {
        options: [],
        pickedOptions: []
    };
    self.officeFilterData = {
        options: [],
        pickedOptions: []
    };
    self.platformClientFilterData = {
        options: [],
        pickedOptions: []
    };
    self.agencyFilterData = {
        options: [],
        pickedOptions: []
    };
    self.billingStatsFilterData = {
        options: [],
        pickedOptions: []
    };
    self.deviceFilterData = {
        options: [],
        pickedOptions: []
    };
    self.clientStatsTable = [];

    let init = function(){
        angular.element(document.getElementsByClassName('tab_link')[0]).addClass('active');
        angular.element(document.getElementById('client_stats_tab')).addClass('active');
    }

    self.openTab = function($event, id) {
        angular.element(document.getElementsByClassName('tab_link')).removeClass('active');
        angular.element($event.target).addClass('active');
        angular.element(document.getElementsByClassName('tab_content')).removeClass('active');
        angular.element(document.getElementById(id)).addClass('active');
    }
    
    self.expandFilterContent = function(){
        angular.element(document.getElementsByClassName('filter_list')).removeClass('active');
        angular.element(document.getElementsByClassName('filter_title')).removeClass('active');
    }

    init();
});

ngModule.factory('dateFilterFactory', function($http){
    let domain = 'http://localhost:9001';
    let dateFilterOptions = null;

    let getDateFilterOptions = function(data){
        let getDateFilterOptionsUrl = '/getDateFilterOptions';
        
        return $http({
            url: domain + getDateFilterOptionsUrl, 
            method: "GET",
            params: {
                date: data
            }
        })
        .then(function (data){
            dateFilterOptions = data.data;
            return Object.assign({}, dateFilterOptions);
        })
        .catch(function(err){
            console.log(err);
            return err;
        });
    }

    let updateDateFilterActiveItem = function(list, id){
        function isNecassary(item) {
            return item === id;
        }

        let objectToReplaceWith = list.find(isNecassary);
        return objectToReplaceWith;
    }

    return {
        getDateFilterOptions: getDateFilterOptions,
        updateDateFilterActiveItem: updateDateFilterActiveItem,
    }
});

ngModule.factory('platformOpsFactory', function($http){
    let domain = 'http://localhost:9001';
    let platformOpsFilterOptions = null;
    
    let getPlatformOpsFilterOptions = function(data){
        let getDateFilterOptionsUrl = '/getPlatformOpsFilterOptions';
        return $http({
            url: domain + getDateFilterOptionsUrl, 
            method: "GET",
            params: {
                "platformOps[]": data
            }
        })
        .then(function (data){
            platformOpsFilterOptions = data.data;
            return Object.assign({}, platformOpsFilterOptions);
        })
        .catch(function(err){
            console.log(err);
            return err;
        });
    }

    return {
        getPlatformOpsFilterOptions: getPlatformOpsFilterOptions,
    }
});

ngModule.factory('regionFactory', function($http){
    let domain = 'http://localhost:9001';
    let regionFilterOptions = null;
    
    let getRegionFilterOptions = function(data){
        let regionFilterOptionsUrl = '/getRegionFilterOptions';
        return $http({
            url: domain + regionFilterOptionsUrl, 
            method: "GET",
            params: {
                "region[]": data
            }
        })
        .then(function (data){
            regionFilterOptions = data.data;
            return Object.assign({}, regionFilterOptions);
        })
        .catch(function(err){
            console.log(err);
            return err;
        });
    }

    return {
        getRegionFilterOptions: getRegionFilterOptions,
    }
});

ngModule.factory('officeFactory', function($http){
    let domain = 'http://localhost:9001';
    let officeFilterOptions = null;
    
    let getOfficeFilterOptions = function(data){
        let officeFilterOptionsUrl = '/getOfficeFilterOptions';
        return $http({
            url: domain + officeFilterOptionsUrl, 
            method: "GET",
            params: {
                "office[]": data
            }
        })
        .then(function (data){
            officeFilterOptions = data.data;
            return Object.assign({}, officeFilterOptions);
        })
        .catch(function(err){
            console.log(err);
            return err;
        });
    }

    return {
        getOfficeFilterOptions: getOfficeFilterOptions,
    }
});

ngModule.factory('platformClientFactory', function($http){
    let domain = 'http://localhost:9001';
    let platformClientFilterOptions = null;
    
    let getPlatformClientFilterOptions = function(data){
        let platformClientFilterOptionsUrl = '/getPlatformClientFilterOptions';
        return $http({
            url: domain + platformClientFilterOptionsUrl, 
            method: "GET",
            params: {
                "platformClient[]": data
            }
        })
        .then(function (data){
            platformClientFilterOptions = data.data;
            return Object.assign({}, platformClientFilterOptions);
        })
        .catch(function(err){
            console.log(err);
            return err;
        });
    }

    return {
        getPlatformClientFilterOptions: getPlatformClientFilterOptions,
    }
});

ngModule.factory('agencyFactory', function($http){
    let domain = 'http://localhost:9001';
    let agencyFilterOptions = null;
    
    let getAgencyFilterOptions = function(data){
        let agencyFilterOptionsUrl = '/getAgencyFilterOptions';
        return $http({
            url: domain + agencyFilterOptionsUrl, 
            method: "GET",
            params: {
                "agency[]": data
            }
        })
        .then(function (data){
            agencyFilterOptions = data.data;
            return Object.assign({}, agencyFilterOptions);
        })
        .catch(function(err){
            console.log(err);
            return err;
        });
    }

    return {
        getAgencyFilterOptions: getAgencyFilterOptions,
    }
});

ngModule.factory('billingStatsFactory', function($http){
    let domain = 'http://localhost:9001';
    let billingStatsFilterOptions = null;
    
    let getBillingStatsFilterOptions = function(data){
        let billingStatsFilterOptionsUrl = '/getBillingStatsFilterOptions';
        return $http({
            url: domain + billingStatsFilterOptionsUrl, 
            method: "GET",
            params: {
                "billingStats[]": data
            }
        })
        .then(function (data){
            billingStatsFilterOptions = data.data;
            return Object.assign({}, billingStatsFilterOptions);
        })
        .catch(function(err){
            console.log(err);
            return err;
        });
    }

    return {
        getBillingStatsFilterOptions: getBillingStatsFilterOptions,
    }
});

ngModule.factory('deviceFactory', function($http){
    let domain = 'http://localhost:9001';
    let deviceFilterOptions = null;
    
    let getDeviceFilterOptions = function(data){
        let deviceFilterOptionsUrl = '/getDeviceFilterOptions';
        return $http({
            url: domain + deviceFilterOptionsUrl, 
            method: "GET",
            params: {
                "device[]": data
            }
        })
        .then(function (data){
            deviceFilterOptions = data.data;
            return Object.assign({}, deviceFilterOptions);
        })
        .catch(function(err){
            console.log(err);
            return err;
        });
    }

    return {
        getDeviceFilterOptions: getDeviceFilterOptions,
    }
});

//improve the name
ngModule.factory('clientStatsFactory', function($http){
    let domain = 'http://localhost:9001';
    let clientStatsTable = null;

    let getClientStatsTable = function(){
        let clientStatsTableUrl = '/getClientStatsTable';
        
        return $http.get(domain + clientStatsTableUrl)
        .then(function(data){
            clientStatsTable = data.data;
            return clientStatsTable;
        })
        .catch(function(err){
            console.log(err);
            return err;
        });
    }

    return {
        getClientStatsTable: getClientStatsTable,
    }
});