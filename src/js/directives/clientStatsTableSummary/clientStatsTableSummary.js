export default function(ngModule){
    ngModule.directive('stSummary', [function () {
        return {
            require: '^stTable',
            replace: true, 
            template: '<p class="summary">{{ stRange.from }} - {{ stRange.to }} of {{ size }}</p>',
            link: function ($scope, $element, $attrs, $stTable) {
                $scope.$watch($stTable.getFilteredCollection, function(val) {
                    $scope.size = (val || []).length;

                    $scope.stRange = {
                        from: null,
                        to: null
                    };

                    $scope.stRange.from = $scope.size ? $stTable.tableState().pagination.start + 1 : $stTable.tableState().pagination.start;
                    $scope.stRange.to = $scope.stItemsByPage > $scope.size ? $scope.size : ($scope.stRange.from + $scope.stItemsByPage - 1);
                });

                $scope.$watch('currentPage', function(){
                    $scope.stRange = {
                        from: null,
                        to: null
                    };

                    $scope.stRange.from = $stTable.tableState().pagination.start + 1;
                    $scope.stRange.to = $scope.currentPage === $scope.numPages ? $scope.size : ($scope.stRange.from + $scope.stItemsByPage - 1);
                });

                $scope.$watch('stItemsByPage',function(){
                    $scope.stRange = {
                        from: null,
                        to: null
                    };

                    $scope.stRange.from = $stTable.tableState().pagination.start + 1;
                    $scope.stRange.to = $scope.stItemsByPage > $scope.size ? $scope.size : ($scope.stRange.from + $scope.stItemsByPage - 1);
                })
            }
        };
    }]);
}