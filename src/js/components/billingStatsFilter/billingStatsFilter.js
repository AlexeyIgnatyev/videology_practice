export default function(ngModule){
    ngModule.component('billingStatsFilter', {
        bindings: {
            click: '&',
            billingstats: '=',
            device: '=',
            table: '='
        },
        templateUrl: './src/js/components/billingStatsFilter/template/billingStatsFilterTemplate.html',
        controller: function($scope, $element, billingStatsFactory){
            let self = this;
            self.$onInit = function() {
                self.billingstats.pickedOptions = []; 
            }
            
            self.changeVisibility = function() {
                self.click();
                $element.children().addClass('active');
            }

            self.applyFilters = function(){
                billingStatsFactory.getBillingStatsFilterOptions(self.billingstats.pickedOptions)
                .then(function(data){
                    console.log(data);
                    self.device.options = data.device;
                    self.device.pickedOptions = [];
                    self.table = data.table;
                })
                .catch(function(err){
                    return err;
                });
            }

            self.clearFilters = function(){
                self.billingstats.pickedOptions = []; 
            }

            self.closeFilterContent = function(){
                $element.children().removeClass('active');
            }
        }
    });    
}