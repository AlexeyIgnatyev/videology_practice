export default function(ngModule){
    ngModule.component('regionFilter', {
        bindings: {
            click: '&',
            region: '=',
            office: '=',
            platformclient: '=',
            agency: '=',
            billingstats: '=',
            device: '=',
            table: '='
        },
        templateUrl: './src/js/components/regionFilter/template/regionFilterTemplate.html',
        controller: function($scope, $element, regionFactory){
            let self = this;
            self.$onInit = function() {
                self.region.pickedOptions = []; 
            }
            
            self.changeVisibility = function() {
                self.click();
                $element.children().addClass('active');
            }

            self.applyFilters = function(){
                regionFactory.getRegionFilterOptions(self.region.pickedOptions)
                .then(function(data){
                    console.log(data);
                    self.office.options = data.office;
                    self.office.pickedOptions = [];
                    self.platformclient.options = data.platformClient;
                    self.platformclient.pickedOptions = [];
                    self.agency.options = data.agency;
                    self.agency.pickedOptions = [];
                    self.billingstats.options = data.billingStats;
                    self.billingstats.pickedOptions = [];
                    self.device.options = data.device;
                    self.device.pickedOptions = [];
                    self.table = data.table;
                })
                .catch(function(err){
                    return err;
                });
            }

            self.clearFilters = function(){
                self.region.pickedOptions = []; 
            }

            self.closeFilterContent = function(){
                $element.children().removeClass('active');
            }
        }
    });    
}