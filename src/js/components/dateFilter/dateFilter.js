export default function(ngModule){
    ngModule.component('dateFilter', {
        bindings: {
            click: '&',
            platformops: '=',
            region: '=',
            office: '=',
            platformclient: '=',
            agency: '=',
            billingstats: '=',
            device: '=',
            table: '='
        },
        templateUrl: './src/js/components/dateFilter/template/dateFilterTemplate.html',
        controller: function($scope, $element, dateFilterFactory){
            let self = this;
            self.$onInit = function() {
                self.dateFilterData = {
                    options: [],
                    activeItem: '',
                }

                dateFilterFactory.getDateFilterOptions()
                .then(function(data) {
                    console.log(data)
                    self.dateFilterData.options= data.date;
                    self.dateFilterData.activeItem = self.dateFilterData.options[0];
                    self.platformops.options = data.platformOps;
                    self.platformops.pickedOptions = [];
                    self.region.options = data.region;
                    self.region.pickedOptions = [];
                    self.office.options = data.office;
                    self.office.pickedOptions = [];
                    self.platformclient.options = data.platformClient;
                    self.platformclient.pickedOptions = [];
                    self.agency.options = data.agency;
                    self.agency.pickedOptions = [];
                    self.billingstats.options = data.billingStats;
                    self.billingstats.pickedOptions = [];
                    self.device.options = data.device;
                    self.device.pickedOptions = [];
                })
                .catch(function(err){
                    return err;
                });
            }

            self.changeVisibility = function() {
                self.click();
                $element.children().addClass('active');
            }

            self.expandDateOptions = function(){
                self.dateFilterOptionsVisibility = !self.dateFilterOptionsVisibility;
            }
            
            self.dateFilterOptionOnChange = function(event) {
                self.dateFilterData.activeItem = dateFilterFactory.updateDateFilterActiveItem(self.dateFilterData.options, event.target.id);
                self.dateFilterOptionsVisibility = false;
                
                dateFilterFactory.getDateFilterOptions(self.dateFilterData.activeItem)
                .then(function(data){
                    console.log(data);
                    self.platformops.options = data.platformOps;
                    self.platformops.pickedOptions = [];
                    self.region.options = data.region;
                    self.region.pickedOptions = [];
                    self.office.options = data.office;
                    self.office.pickedOptions = [];
                    self.platformclient.options = data.platformClient;
                    self.platformclient.pickedOptions = [];
                    self.agency.options = data.agency;
                    self.agency.pickedOptions = [];
                    self.billingstats.options = data.billingStats;
                    self.billingstats.pickedOptions = [];
                    self.device.options = data.device;
                    self.device.pickedOptions = [];
                    self.table = data.table;
                })
                .catch(function(err){
                    return err;
                });
            }
        }
    });    
}