export default function(ngModule){
    ngModule.component('platformOpsFilter', {
        bindings: {
            click: '&',
            platformops: '=',
            region: '=',
            office: '=',
            platformclient: '=',
            agency: '=',
            billingstats: '=',
            device: '=',
            table: '='
        },
        templateUrl: './src/js/components/platformOpsFilter/template/platformOpsFilterTemplate.html',
        controller: function($scope, $element, platformOpsFactory){
            let self = this;
            
            self.$onInit = function() {
                self.platformops.pickedOptions = [];
            }

            self.changeVisibility = function() {
                self.click();
                $element.children().addClass('active');
            }

            self.applyFilters = function(){
                platformOpsFactory.getPlatformOpsFilterOptions(self.platformops.pickedOptions)
                .then(function(data){
                    self.region.options = data.region;
                    self.region.pickedOptions = [];
                    self.office.options = data.office;
                    self.office.pickedOptions = [];
                    self.platformclient.options = data.platformClient;
                    self.platformclient.pickedOptions = [];
                    self.agency.options = data.agency;
                    self.agency.pickedOptions = [];
                    self.billingstats.options = data.billingStats;
                    self.billingstats.pickedOptions = [];
                    self.device.options = data.device;
                    self.device.pickedOptions = [];
                    self.table = data.table;
                })
                .catch(function(err){
                    return err;
                });
            }

            self.clearFilters = function(){
                self.platformops.pickedOptions = []; 
            }
     
            self.closeFilterContent = function(){
                $element.children().removeClass('active');
            }
        }
    });    
}