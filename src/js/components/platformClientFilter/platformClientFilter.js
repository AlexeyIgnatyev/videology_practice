export default function(ngModule){
    ngModule.component('platformClientFilter', {
        bindings: {
            click: '&',
            platformclient: '=',
            agency: '=',
            billingstats: '=',
            device: '=',
            table: '='
        },
        templateUrl: './src/js/components/platformClientFilter/template/platformClientFilterTemplate.html',
        controller: function($scope, $element, platformClientFactory){
            let self = this;
            self.$onInit = function() {
                self.platformclient.pickedOptions = []; 
            }

            self.changeVisibility = function() {
                self.click();
                $element.children().addClass('active');
            }
            
            self.applyFilters = function(){
                platformClientFactory.getPlatformClientFilterOptions(self.platformclient.pickedOptions)
                .then(function(data){
                    console.log(data);
                    self.agency.options = data.agency;
                    self.agency.pickedOptions = [];
                    self.billingstats.options = data.billingStats;
                    self.billingstats.pickedOptions = [];
                    self.device.options = data.device;
                    self.device.pickedOptions = [];
                    self.table = data.table;
                })
                .catch(function(err){
                    return err;
                });
            }

            self.clearFilters = function(){
                self.platformclient.pickedOptions = []; 
            }

            self.closeFilterContent = function(){
                $element.children().removeClass('active');
            }
        }
    });    
}