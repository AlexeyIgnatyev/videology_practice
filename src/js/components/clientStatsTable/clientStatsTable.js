import summary from '../../directives/clientStatsTableSummary/clientStatsTableSummary.js';

export default function(ngModule){
    summary(ngModule);
    ngModule.component('clientStatsTable', {
        bindings: {
            table: '='
        },
        templateUrl: './src/js/components/clientStatsTable/template/clientStatsTableTemplate.html',
        controller: function($scope, $element, $attrs, clientStatsFactory){
            let self = this;
            self.$onInit = function() {
                console.log(self.table)
                self.isLoading = true;
                clientStatsFactory.getClientStatsTable()
                .then(function(data){
                    self.table = data;
                    self.displayedTable = [...self.table];
                    self.isLoading = !self.isLoading;
                })
                .catch(function(err){
                    return err;
                });
            }
    
            self.options = [
                {value: 5, name: '5'},
                {value: 10, name: '10'},
                {value: 20, name: '20'},
                {value: 100, name: '100'},
            ];
    
            self.selectedValue = self.options[0].value;
            self.itemsByPage = self.options[0].value;
    
            self.onChange = function(){
                self.itemsByPage = self.selectedValue;
            }
        },
    });
}