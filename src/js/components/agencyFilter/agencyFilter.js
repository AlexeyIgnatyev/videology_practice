export default function(ngModule){
    ngModule.component('agencyFilter', {
        bindings: {
            click: '&',
            agency: '=',
            billingstats: '=',
            device: '=',
            table: '='
        },
        templateUrl: './src/js/components/agencyFilter/template/agencyFilterTemplate.html',
        controller: function($scope, $element, agencyFactory){
            let self = this;
            self.$onInit = function() {
                self.agency.pickedOptions = []; 
            }
            
            self.changeVisibility = function() {
                self.click();
                $element.children().addClass('active');
            }

            self.applyFilters = function(){
                agencyFactory.getAgencyFilterOptions(self.agency.pickedOptions)
                .then(function(data){
                    console.log(data);
                    self.billingstats.options = data.billingStats;
                    self.billingstats.pickedOptions = [];
                    self.device.options = data.device;
                    self.device.pickedOptions = [];
                    self.table = data.table;
                })
                .catch(function(err){
                    return err;
                });
            }
            
            self.clearFilters = function(){
                self.agency.pickedOptions = []; 
            }

            self.closeFilterContent = function(){
                $element.children().removeClass('active');
            }
        }
    });    
}