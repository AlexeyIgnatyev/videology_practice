export default function(ngModule){
    ngModule.component('officeFilter', {
        bindings: {
            click: '&',
            office: '=',
            platformclient: '=',
            agency: '=',
            billingstats: '=',
            device: '=',
            table: '='
        },
        templateUrl: './src/js/components/officeFilter/template/officeFilterTemplate.html',
        controller: function($scope, $element, officeFactory){
            let self = this;
            self.$onInit = function() {
                self.office.pickedOptions = []; 
            }

            self.changeVisibility = function() {
                self.click();
                $element.children().addClass('active');
            }

            self.applyFilters = function(){
                officeFactory.getOfficeFilterOptions(self.office.pickedOptions)
                .then(function(data){
                    console.log(data);
                    self.platformclient.options = data.platformClient;
                    self.platformclient.pickedOptions = [];
                    self.agency.options = data.agency;
                    self.agency.pickedOptions = [];
                    self.billingstats.options = data.billingStats;
                    self.billingstats.pickedOptions = [];
                    self.device.options = data.device;
                    self.device.pickedOptions = [];
                    self.table = data.table;
                })
                .catch(function(err){
                    return err;
                });
            }

            self.clearFilters = function(){
                self.office.pickedOptions = []; 
            }

            self.closeFilterContent = function(){
                $element.children().removeClass('active');
            }
        }
    });    
}