export default function(ngModule){
    ngModule.component('deviceFilter', {
        bindings: {
            click: '&',
            device: '=',
            table: '='
        },
        templateUrl: './src/js/components/deviceFilter/template/deviceFilterTemplate.html',
        controller: function($scope, $element, deviceFactory){
            let self = this;
            self.$onInit = function() {
                self.device.pickedOptions = []; 
            }
            
            self.changeVisibility = function() {
                self.click();
                $element.children().addClass('active');
            }

            self.applyFilters = function(){
                deviceFactory.getDeviceFilterOptions(self.device.pickedOptions)
                .then(function(data){
                    console.log(data);
                    self.table = data.table;
                })
                .catch(function(err){
                    return err;
                });
            }

            self.clearFilters = function(){
                self.device.pickedOptions = []; 
            }

            self.closeFilterContent = function(){
                $element.children().removeClass('active');
            }
        }
    });    
}